
#!/usr/bin/env bash

cat <<EOF
include: '/gci-templates/gitlab-ci.yml'

stages:
  - test
  - install
  - release

EOF

for f in $(find charts/* -maxdepth 0 -type d)
do
  CHART_VERSION=$(grep "^version:" ${f}/Chart.yaml | cut -d' ' -f2-)
  CHART_RELEASE="${f##*/}-${CHART_VERSION}"
  cat <<EOF
'${f##*/}:lint':
  stage: test
  extends: .lint
  variables:
    HELM_NAME: "${f##*/}"
  rules:
    - if: '\$CI_COMMIT_TAG =~ "/^$/"'
      # changes:
        # - ${f}/**/*

'${f##*/}:release':
  stage: release
  extends: .release
  variables:
    HELM_NAME: "${f##*/}"
  rules:
    - if: '\$CI_COMMIT_TAG =~ "/^[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}$/"'

EOF

done
